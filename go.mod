module github.com/marten-seemann/qtls-go1-16

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.0
	github.com/golang/mock v1.6.0
	github.com/tjfoc/gmsm v1.4.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	golang.org/x/sys v0.0.0-20220222200937-f2425489ef4c
)
